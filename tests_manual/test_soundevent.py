import os
import sys
sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))
from hippasos.soundevent import SoundEvent
from pelops import myconfigtools, mymqttclient
from pelops.logging import mylogger
import pygame
import time

config = myconfigtools.read_config("../tests_unit/config.yaml")

logger = mylogger.create_logger(config["logger"], __name__)
mqtt_client = mymqttclient.MyMQTTClient(config["mqtt"], logger)
mqtt_client.connect()
pygame.init()
pygame.mixer.init()

se = SoundEvent(config["sound-mappings"][1], mqtt_client, logger, pygame.mixer)
se.start()
se.play()


time.sleep(10)
se.stop()
mqtt_client.disconnect()
