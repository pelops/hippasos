import os
import sys
sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))
from hippasos.soundservice import SoundService
from pelops import myconfigtools, mymqttclient
from pelops.logging import mylogger
import time

config = myconfigtools.read_config("../tests_unit/config.yaml")

logger = mylogger.create_logger(config["logger"], __name__)
mqtt_client = mymqttclient.MyMQTTClient(config["mqtt"], logger)

topic_pub_bell = "/test/button1"
topic_pub_ping = "/test/button3"
topic_pub_flat = "/test/button2"
pub_message = "PRESSED"

print("SoundService.start ===========================================")

ss = SoundService(config, mqtt_client, logger, no_gui=True)
ss.start()
time.sleep(1)

print("play bell x1 ----------------------------------------------------")
mqtt_client.publish(topic_pub_bell, pub_message)
time.sleep(15)
print("play ping x1 ----------------------------------------------------")
mqtt_client.publish(topic_pub_ping, pub_message)
time.sleep(4)
print("play flat x1 ----------------------------------------------------")
mqtt_client.publish(topic_pub_flat, pub_message)
time.sleep(4)
print("play flat, bell, ping x1 ----------------------------------------------------")
mqtt_client.publish(topic_pub_bell, pub_message)
mqtt_client.publish(topic_pub_flat, pub_message)
mqtt_client.publish(topic_pub_ping, pub_message)
time.sleep(15)
print("play bell x3 ----------------------------------------------------")
mqtt_client.publish(topic_pub_ping, pub_message)
time.sleep(1)
mqtt_client.publish(topic_pub_ping, pub_message)
time.sleep(1)
mqtt_client.publish(topic_pub_ping, pub_message)
time.sleep(4)

print("SoundService.stop ===========================================")
ss.stop()